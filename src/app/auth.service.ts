import { Injectable } from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';
import {AngularFireAuth} from '@angular/fire/auth';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  signup(email: string, password: string){
    return this.firebaseAuth
    .auth
    .createUserWithEmailAndPassword(email,password)
  }
  login(email:string,password:string){
    return this.firebaseAuth.auth.signInWithEmailAndPassword(email,password)
  }
  logout(){
    return this.
    firebaseAuth.
    auth.signOut()
  }
  updateProfile(user,name){
    user.updateProfile({displayName:name,photoURL:''})
  }
  addUser(user,name){
    let uid = user.uid;
    let ref = this.db.database.ref('/');        
    ref.child('user').child(uid).push({name:name}); 
  }
  user:Observable<firebase.User>;
  constructor(private firebaseAuth: AngularFireAuth, private db:AngularFireDatabase) { 
    this.user = firebaseAuth.authState;

  }
}
