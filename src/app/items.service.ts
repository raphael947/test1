import { Injectable } from '@angular/core';
import {AuthService } from './auth.service';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ItemsService {

  delete(key)  {
    this.authService.user.subscribe(
      user =>{
        console.log(user.uid);
        let uid = user.uid;
        this.db.list('users/'+uid +'/items').remove(key);
      }
    )
  } 
  
  updateInevntory(key,isInInventory){
    console.log(key);
    console.log(isInInventory);
    this.authService.user.subscribe(
      user =>{
        let uid = user.uid;
        this.db.list('users/'+uid +'/items').update(key,{inventory:isInInventory});
      }
    )   
  }

  constructor(private authService:AuthService, private db:AngularFireDatabase) { }



}