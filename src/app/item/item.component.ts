import { Component, OnInit, Input } from '@angular/core';
import {ItemsService} from  '../items.service';

@Component({
  selector: 'item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.css']
})
export class ItemComponent implements OnInit {
  @Input() data:any;

  name;
  price;
  $key;
  showdelete:boolean = false;
  areYouSure:boolean = false;
  isInInventory:boolean = false;


  delete(){
    console.log(this.$key)
    this.itemsService.delete(this.$key)
  }

  show(){
    if(!this.areYouSure)
    this.showdelete = true;
  }

  hide(){
    this.showdelete = false;
  }  

  showAreYouSure(){
     this.areYouSure = true;
     this.showdelete = false;
  }

  hideAreYouSure(){
    this.areYouSure = false;
  }  

  changeInventory(){
    console.log(this.isInInventory);
    this.itemsService.updateInevntory(this.$key, this.isInInventory);
  }

  constructor(private itemsService:ItemsService) { }

  ngOnInit() {
    this.$key = this.data.$key;     
    this.name = this.data.name;
    this.price = this.data.price;
    this.isInInventory = this.data.inventory;  
  }

}