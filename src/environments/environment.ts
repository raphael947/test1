// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDOAJwr2ZSqtU9-UjMX7NhP_d6g8v_HA3k",
    authDomain: "test1-2c720.firebaseapp.com",
    databaseURL: "https://test1-2c720.firebaseio.com",
    projectId: "test1-2c720",
    storageBucket: "test1-2c720.appspot.com",
    messagingSenderId: "1067283468099"
  
}
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
